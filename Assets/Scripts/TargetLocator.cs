using UnityEngine;

public class TargetLocator : MonoBehaviour
{
    [SerializeField] private Transform weapon;
    [SerializeField] private float towerRange = 15f;
    [SerializeField] private ParticleSystem projectileParticles;
    private Transform _target;
    
    void Update()
    {
        FindClosestTarget();
        AimWeapon();
    }

    private void FindClosestTarget()
    {
        Enemy[] enemies = FindObjectsOfType<Enemy>();
        Transform closestEnemy = null;
        float maxDistance = Mathf.Infinity;

        foreach (Enemy enemy in enemies)
        {
            float targetDistance = Vector3.Distance(transform.position, enemy.transform.position);
            if (targetDistance < maxDistance)
            {
                closestEnemy = enemy.transform;
                maxDistance = targetDistance;
            }
        }
        
        _target = closestEnemy;
    }

    void AimWeapon()
    {
        if (_target == null) return;
        float targetDistance = Vector3.Distance(transform.position, _target.position);
        if (towerRange > targetDistance)
        {
            weapon.transform.LookAt(_target, Vector3.up);
            Attack(true);
        }
        else
        {
            Attack(false);
        }
    }

    void Attack(bool isActive)
    {
        ParticleSystem.EmissionModule emission = projectileParticles.emission;
        emission.enabled = isActive;
    }
}
