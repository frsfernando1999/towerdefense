using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyMover : MonoBehaviour
{
    [Header("Speed Controls")]
    [Tooltip("Speed for running the path. The higher the number the faster it goes")]
    [SerializeField]
    [Range(0, 5)]
    private float speed = 1;

    [Tooltip("Speed for rotating the model on corners. The higher the number, the faster it turns")]
    [SerializeField]
    [Range(2, 5)]
    private float rotationSpeed = 4;

    [Header("Tiles used for creating the main path")]
    private List<Node> _path = new List<Node>();

    private GridManager _gridManager;
    private Pathfinder _pathfinder;

    private Enemy _enemy;

    void OnEnable()
    {
        ReturnToStart();
        RecalculatePath(true);
    }

    private void Awake()
    {
        _enemy = GetComponent<Enemy>();
        _gridManager = FindObjectOfType<GridManager>();
        _pathfinder = FindObjectOfType<Pathfinder>();
    }

    void ReturnToStart()
    {
        transform.position = _gridManager.GetPositionFromCoordinates(_pathfinder.StartCoordinates);
    }

    void RecalculatePath(bool resetPath)
    {
        Vector2Int coordinates = new Vector2Int();
        if (resetPath )
        {
            coordinates = _pathfinder.StartCoordinates;
        }
        else
        {
            coordinates = _gridManager.GetCoordinatesFromPosition(transform.position);
        }
        StopAllCoroutines();
        _path.Clear();
        _path = _pathfinder.GetNewPath(coordinates);
        StartCoroutine(FollowPath());

    }

    IEnumerator FollowPath()
    {
        for (int i = 1; i < _path.Count; i++)
        {
            Vector3 startPosition = transform.position;
            Vector3 endPosition = _gridManager.GetPositionFromCoordinates(_path[i].coordinates);
            float travelPercent = 0f;

            float rotationDegrees = Vector3.SignedAngle(transform.forward, endPosition - startPosition, Vector3.up);

            while (travelPercent < 1)
            {
                if (travelPercent < 1 / rotationSpeed)
                {
                    float rotationAngle = rotationSpeed * rotationDegrees * Time.deltaTime * speed;
                    transform.Rotate(new Vector3(0, rotationAngle, 0));
                }

                travelPercent += Time.deltaTime * speed;
                transform.position = Vector3.Lerp(startPosition, endPosition, travelPercent);
                yield return new WaitForEndOfFrame();
            }
        }
        FinishPath();
    }

    private void FinishPath()
    {
        gameObject.SetActive(false);
        _enemy.StealGold();
    }
}