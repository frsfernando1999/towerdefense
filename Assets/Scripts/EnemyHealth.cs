using UnityEngine;


[RequireComponent(typeof(Enemy))]
public class EnemyHealth : MonoBehaviour
{
    [SerializeField] private int maxHealth = 5;
    [SerializeField] private int difficultyRamp = 1;
    private int _health = 5;
    
    private Enemy _enemy;

    private void Start()
    {
        _enemy = GetComponent<Enemy>();
    }

    private void OnEnable()
    {
        _health = maxHealth;
    }

    private void OnParticleCollision(GameObject other)
    {
        // Do not forget to check "Send collision message" on the particle system!
        _health--;
        if (_health > 0) return;
        gameObject.SetActive(false);
        maxHealth += difficultyRamp;
        _enemy.RewardGold();
    }
}
