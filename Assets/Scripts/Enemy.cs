using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] [Range(0, 1000)] private int goldReward = 25;
    [SerializeField] [Range(0, 1000)] private int goldPenalty = 25;

    private Bank _bank;

    private void Start()
    {
        _bank = FindObjectOfType<Bank>();
    }

    public void RewardGold()
    {
        if (_bank == null) return;
        
        _bank.ModifyBalance(goldReward);
    }
    
    public void StealGold()
    {
        if (_bank == null) return;
        
        _bank.ModifyBalance(-goldPenalty);
    }
    
}
