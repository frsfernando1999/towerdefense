using System;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [SerializeField] private bool isPlaceable;

    public bool IsPlaceable
    {
        get { return isPlaceable; }
    }


    [SerializeField] private Tower towerPrefab;

    private GridManager _gridManager;
    private Pathfinder _pathfinder;
    private Vector2Int coordinates = new Vector2Int();

    private void Awake()
    {
        _gridManager = FindObjectOfType<GridManager>();
        _pathfinder = FindObjectOfType<Pathfinder>();
    }

    private void Start()
    {
        if (_gridManager != null)
        {
            coordinates = _gridManager.GetCoordinatesFromPosition(transform.position);

            if (!isPlaceable)
            {
                _gridManager.BlockNode(coordinates);
            }
        }
    }

    private void OnMouseDown()
    {
        if (_gridManager.GetNode(coordinates).isWalkable && !_pathfinder.WillBlockPath(coordinates))
        {
            bool isSuccessful = towerPrefab.CreateTower(towerPrefab, transform.position);
            if (!isSuccessful) return;
            _gridManager.BlockNode(coordinates);
            _pathfinder.NotifyReceivers();
        }
    }
}