using TMPro;
using Unity.VisualScripting;
using UnityEngine;

[ExecuteAlways]
[RequireComponent(typeof(TextMeshPro))]
public class Coordinate : MonoBehaviour
{
    [SerializeField] private Color defaultColor = Color.magenta;
    [SerializeField] private Color blockedColor = Color.grey;
    [SerializeField] private Color exploredColor = new Color(1f, 0.5f, 0.0f);
    [SerializeField] private Color pathColor = Color.blue;


    private TextMeshPro _label;
    private Vector2Int _coordinates;
    private GridManager _gridManager;

    private void Awake()
    {
        _gridManager = FindObjectOfType<GridManager>();
        _label = GetComponent<TextMeshPro>();
        _label.enabled = false;
        DisplayCoordinates();
    }

    private void Update()
    {
        if (!Application.isPlaying)
        {
            DisplayCoordinates();
            UpdateObjectName();
            _label.enabled = true;
        }

        CoordinatesTextColor();
        ToggleLabels();
    }

    void ToggleLabels()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            _label.enabled = !_label.IsActive();
        }
    }

    private void CoordinatesTextColor()
    {
        if (_gridManager == null) return;
        Node node = _gridManager.GetNode(_coordinates);

        if (node == null) return;

        if (!node.isWalkable)
        {
            _label.color = blockedColor;
        }
        else if (node.isPath)
        {
            _label.color = pathColor;
        }
        else if (node.isExplored)
        {
            _label.color = exploredColor;
        }
        else
        {
            _label.color = defaultColor;
        }
    }

    private void DisplayCoordinates()
    {
        // If you try to package a game with UnityEditor variables, the build will fail.
        // Move this file a folder named "Editor" that is going to be ignored by the compiler when building
        var position = transform.parent.position;

        if (_gridManager == null) return;
        
        _coordinates.x = Mathf.RoundToInt(position.x / _gridManager.UnityWorldGridSize);
        _coordinates.y = Mathf.RoundToInt(position.z / _gridManager.UnityWorldGridSize);

        _label.text = $"{_coordinates.x}, {_coordinates.y}";
    }

    void UpdateObjectName()
    {
        transform.parent.name = _coordinates.ToString();
    }
}