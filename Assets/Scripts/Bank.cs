using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bank : MonoBehaviour
{
    [SerializeField] private int startingBalance = 150;
    public int CurrentBalance { get; private set; }

    private Canvas _hud;
    [SerializeField] private TextMeshProUGUI goldText;
    

    private void Awake()
    {
        CurrentBalance = startingBalance;
    }

    private void Start()
    {
        UpdateGoldHUD();
    }

    public void ModifyBalance(int amount)
    {
        CurrentBalance += amount;
        UpdateGoldHUD();
        if (CurrentBalance < 0)
        {
            Invoke(nameof(LoseGame), 1f);
        }
    }

    private void LoseGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    private void UpdateGoldHUD()
    {
        goldText.text = $"Gold: {CurrentBalance}";
    }


}