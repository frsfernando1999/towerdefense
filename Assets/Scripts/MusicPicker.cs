using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class MusicPicker : MonoBehaviour
{
    [SerializeField] private List<AudioClip> musicList;

    private int _currentMusic;
    private AudioSource _audioSource;

    private void Awake()
    {
        int numMusicPlayers = FindObjectsOfType<MusicPicker>().Length;
        if (numMusicPlayers > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        
        var randomMusic = Random.Range(0, musicList.Count);
        _currentMusic = randomMusic;

        _audioSource.clip = musicList[randomMusic];
        _audioSource.Play();

        Invoke(nameof(SwapMusic), musicList[randomMusic].length + 2f);
    }

    private void SwapMusic()
    {
        var randomMusic = Random.Range(0, musicList.Count);
        while (randomMusic == _currentMusic)
        {
            randomMusic = Random.Range(0, musicList.Count);
        }

        _currentMusic = randomMusic;
        _audioSource.clip = musicList[randomMusic];
        _audioSource.Play();
        
        Invoke(nameof(SwapMusic), musicList[randomMusic].length + 2f);
    }
}