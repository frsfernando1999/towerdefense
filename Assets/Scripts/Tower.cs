using System.Collections;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField] private int towerCost = 75;
    [SerializeField] private float buildDelay = 0.5f;

    private void Start()
    {
        StartCoroutine(Build());
    }

    private IEnumerator Build()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
            foreach (Transform grandchild in child)
            {
                grandchild.gameObject.SetActive(false);
            }
        }
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
            yield return new WaitForSeconds(buildDelay);
            foreach (Transform grandchild in child)
            {
                grandchild.gameObject.SetActive(true);
            }
        }
    }

    public bool CreateTower(Tower tower, Vector3 transformPos)
    {
        Bank bank = FindObjectOfType<Bank>();
        if (bank == null) return false;

        if (bank.CurrentBalance >= towerCost)
        {
            Instantiate(tower.gameObject, transformPos, Quaternion.identity);
            bank.ModifyBalance(-towerCost);
            return true;
        }

        return false;
    }
}