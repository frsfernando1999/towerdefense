using System.Collections;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] [Range(0.1f, 30f)] private float spawnDelay = 1f;
    [SerializeField] [Range(0, 50)] private int poolSize = 5;
    [SerializeField] private GameObject enemyToSpawn;

    private GameObject[] pool;

    private void Awake()
    {
        PopulatePool();
    }

    void Start()
    {
        StartCoroutine(SpawnEnemy());
    }

    private void PopulatePool()
    {
        pool = new GameObject[poolSize];

        for (int i = 0; i < pool.Length; i++)
        {
            pool[i] = Instantiate(enemyToSpawn, transform);
            pool[i].SetActive(false);
        }
    }


    IEnumerator SpawnEnemy()
    {
        while (true)
        {
            for (int i = 0; i < pool.Length; i++)
            {
                if (!pool[i].activeSelf)
                {
                    pool[i].SetActive(true);
                    yield return new WaitForSeconds(spawnDelay);
                }
            }

            yield return new WaitForSeconds(spawnDelay);
        }
    }
}