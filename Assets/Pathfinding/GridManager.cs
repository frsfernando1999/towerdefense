using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    [SerializeField] private Vector2Int gridSize;
    
    [Tooltip("Should match the Unity editor snap settings")]
    [SerializeField] private int unityWorldGridSize = 10;
    public int UnityWorldGridSize { get { return unityWorldGridSize; } }
    private Dictionary<Vector2Int, Node> _grid = new Dictionary<Vector2Int, Node>();
    public Dictionary<Vector2Int, Node> Grid
    {
        get { return _grid;  }
    }

    private void Awake()
    {
        CreateGrid();
    }

    public Node GetNode(Vector2Int coordinate)
    {
        if (_grid.ContainsKey(coordinate))
        {
            return _grid[coordinate];
        }
        return null;
    }

    public void BlockNode(Vector2Int coordinates)
    {
        if (_grid.ContainsKey(coordinates))
        {
            _grid[coordinates].isWalkable = false;
        }
    }

    public void ResetNodes()
    {
        foreach (KeyValuePair<Vector2Int, Node> entry in _grid)
        {
            entry.Value.connectedTo = null;
            entry.Value.isExplored = false;
            entry.Value.isPath = false;
        }
    }

    public Vector2Int GetCoordinatesFromPosition(Vector3 position)
    {
        Vector2Int coordinates = new Vector2Int();
        coordinates.x = Mathf.RoundToInt(position.x / unityWorldGridSize);
        coordinates.y = Mathf.RoundToInt(position.z / unityWorldGridSize);

        return coordinates;
    }

    public Vector3 GetPositionFromCoordinates(Vector2 coordinates)
    {
        Vector3 position = new Vector3();
        position.x = Mathf.RoundToInt(coordinates.x * unityWorldGridSize);
        position.z = Mathf.RoundToInt(coordinates.y * unityWorldGridSize);

        return position;
    }

    private void CreateGrid()
    {
        for (int i = 0; i < gridSize.x; i++)
        {
            for (int j = 0; j < gridSize.y; j++)
            {
                Vector2Int coordinates = new Vector2Int(i, j);
                _grid.Add(coordinates, new Node(coordinates, true));
                
            }
        }
    }
}